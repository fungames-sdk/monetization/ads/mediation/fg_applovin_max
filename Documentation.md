# Applovin Max

## Integration Steps

1) **"Install"** or **"Upload"** FG ApplovinMax plugin from the FunGames Integration Manager in Unity, or download it from here<.

2) Follow the instructions in the **"Install External Plugin"** section to import ApplovinMax SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) Follow the **Account and Settings** section.

5) To finish your integration, install the mandatory ad networks according to the **Install mediation networks** section.

## Install External Plugin

After importing the FG ApplovinMax module from the Integration Manager window, you will find the last compatible version of ApplovinMax SDK in the _Assets > FunGames_Externals > Mediation_ folder. Double click on the .unitypackage file to install it.

If you wish to install a newest version of their SDK, you can also download the <a href="https://dash.applovin.com/documentation/mediation/unity/getting-started/integrationhttps:/dash.applovin.com/documentation/mediation/unity/getting-started/integration" target="_blank">Max Unity Plugin</a>.

**Note that the versions of included external SDKs are the latest tested and approved by our development team. We recommend using these versions to avoid any type of discrepencies. If, for any reason, you already have a different version of the plugin installed in your project that you wish to keep, please advise our dev team.**


## Account and Settings

You can configure Applovin Max through the **Applovin Integration Manager** (in the menu toolbar Applovin -> Integration Manager) . 

Enable MAX Ad Review in the AppLovin Quality Service section. 

For the ads to work you need to go to the **FG ApplovinMax Settings** (_Assets > Resources > FunGames > FGApplovinMaxSettings_) and fill in the different keys. To do so you need to follow these steps:

- Create a build, just initializing the Sdk (without the Ad Units Id) and run it on
your device (real device)
- The ad unit ids will then be created on our backend. You need to ask the publishing
team for those ad unit ids.
- Once you have those units ids, please integrate them in the FGMax Settings in the
Ressource Folder.
- When you Ad Unit Ids are correctly setup, you can built and run your app on a mobile device to test their integration.
- You can also use the Max Debugger to display information about your Max configuration in game by calling the MaxSdk.ShowMediationDebugger() function.

## Install mediation networks

Now that ApplovinMax is properly set up through FG SDK, you will be able to install all the Ad networks from the **Applovin Integration Manager**.

This is the list of mandatory networks you will need to integrate :

- Admob
- Google Ad Manager
- Meta Audience Network
- DT Exchange
- IronSource
- LiftOff Monetize
- Pangle
- InMobi
- Mintegral
- Unity Ads
- Verve

Some of these networks will require some specific instructions to be properly installed. Please check the <a href=" https://dash.applovin.com/documentation/mediation/unity/mediation-adapters" target="_blank">Applovin Documention</a> to make sure everything is well setup in your project.

## AppOpen

AppOpen is a recent feature from Applovin MAX SDK that enables to display Ads on app init or when the app was in background and opened again. 

Contrary to other type of Ads, AppOpen is triggered automatically in FG sdk (no need to call the "Show" function) according to some conditions that can be set by code.

By default we defined 3 conditions and 2 of them can be handled directly with RemoteConfig values:

- AppOpen will never be displayed at first connection
- AppOpen frequency can be set using the **AppOpenFrequency** remote config. (ex : if AppOpenFrequency is set to 1, an ad will be displayed at every OnApplicationPause(false) call. If its set to 2, then the ad will be displayed every 2 OnApplicationPause(false) calls, etc.)
- AppOpen cooldown can be set using the **AppOpenCooldown** remote config. (ex : if AppOpenCooldown is set to 30, AppOpen ads will only display if no other AppOpen ad was shown in the last 30sec)

All these condition are combined through an AND operator, so that AppOpen Ad is displayed only if ALL conditions matches.

You can also add your custom conditions using the _FGMediation.AddAppOpenCondition(...)_ method.

To learn more about AppOpen best practices please refer to the <a href="https://dash.applovin.com/documentation/mediation/ios/ad-formats/app-open" target="_blank">Applovin MAX documentation</a>.

## Multiple Ad Units

> ![](_source/applovin_AdditionalUnits.png)

Since FGApplovinMax v3.2.2, if you want to use multiple Ad Unit IDs in your Ad mediation, you can add it to the FGApplovinMaxSettings.

You will then be able to show an ad with specific AdId using the _FGMediation.ShowInterstitialWithId_ , _FGMediation.ShowRewardedWithId_ and _FGMediation.ShowBannerWithId_ methods.

When showing an Ad with a given UnitID, if no ad from the specific ID is ready (loaded), the SDK will use the default UnitID instead, to maximise the chances to get impressions from this placement.